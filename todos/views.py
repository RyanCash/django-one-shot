from django.shortcuts import render, get_object_or_404
from todos.models import TodoList, TodoItem

# Create your views here.
# create a view that will get all the instances of todolist and put them in a context for the template


def show_todolist(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/todolist.html", context)


def todo_detail(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {"todo_detail": todo_detail}
    return render(request, "todos/todo_list_detail.html", context)
