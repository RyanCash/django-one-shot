from django.urls import path
from todos.views import show_todolist, todo_detail

urlpatterns = [
    path("<int:id>/", todo_detail, name="todo_detail"),
    path("", show_todolist, name="show_todolist"),
]
